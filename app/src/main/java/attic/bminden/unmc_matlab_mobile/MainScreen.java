package attic.bminden.unmc_matlab_mobile;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.VideoView;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainScreen extends AppCompatActivity implements View.OnClickListener {

    Button btnCalibrate, btnCapture, btnProcess, btnNew;
    Globals g;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);

        Bundle data = getIntent().getExtras();
        g = data.getParcelable("Globals");

        setTitle("Subject ID: " + String.format("%04d",g.getSubjectID()) + " Trial: " + String.format("%04d",(g.getTrialNum())));

        /**btnCalibrate = findViewById(R.id.calibrate_btn);
        btnCalibrate.setOnClickListener(this);**/
        btnCapture = findViewById(R.id.capture_btn);
        btnCapture.setOnClickListener(this);
        btnProcess = findViewById(R.id.process_btn);
        btnProcess.setOnClickListener(this);
        btnNew = findViewById(R.id.new_btn);
        btnNew.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        /** Calibrate Button
        if(v.getId() == R.id.calibrate_btn){
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, 0);
            }
        }**/
        /** Capture Button **/
        if(v.getId() == R.id.capture_btn){

            Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            /** Need to fix the EXTRA_OUTPUT **/
            if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(takeVideoIntent, 1);
            }
        }
        /** Process Button **/
        if(v.getId() == R.id.process_btn){
            Intent in = new Intent(MainScreen.this, ProcessJava.class);
            in.putExtra("Globals", g);
            startActivity(in);
        }
        /** New Session Button **/
        if(v.getId() == R.id.new_btn){
            Intent in = new Intent(MainScreen.this, CloseSessionConfirm.class);
            startActivity(in);
        }
    }

    @Override
    public void onBackPressed() {
        /** Ignore it **/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        /** What to do after video is captured and confirmed **/
        if (resultCode == MainScreen.RESULT_OK && requestCode == 1) {
            Uri videoUri = data.getData();
        }
    }
}