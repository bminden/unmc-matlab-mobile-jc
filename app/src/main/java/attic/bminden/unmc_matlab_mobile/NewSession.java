package attic.bminden.unmc_matlab_mobile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;

public class NewSession extends AppCompatActivity implements View.OnClickListener {

    Globals g;
    Button btnCreate;
    TextView subjectIDEntry, trialNumEntry;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_session);

        g = new Globals();

        btnCreate = findViewById(R.id.create_btn);
        btnCreate.setOnClickListener(this);

        subjectIDEntry = findViewById(R.id.subject_id_entry);
        trialNumEntry = findViewById(R.id.trial_num_entry);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.create_btn){
            g.setSubjectID(Integer.valueOf(String.valueOf(subjectIDEntry.getText())));
            g.setTrialNum(Integer.valueOf(String.valueOf(trialNumEntry.getText())));
            g.setLocation("../data/data/attic.bminden.unmc_matlab_mobile/files/" + String.valueOf(subjectIDEntry.getText()) + "/" + String.valueOf(trialNumEntry.getText()) + "/");

            createFolders(new File(g.getLocation()));

            Intent in = new Intent(NewSession.this, MainScreen.class);
            in.putExtra("Globals", g);
            startActivity(in);
        }
    }

    public void createFolders(File loc){
        if(!loc.exists()){
            loc.mkdirs();
        }
    }
}
