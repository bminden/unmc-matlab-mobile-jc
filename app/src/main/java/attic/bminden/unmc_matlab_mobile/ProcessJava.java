package attic.bminden.unmc_matlab_mobile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class ProcessJava extends AppCompatActivity implements View.OnClickListener {

    TextView output;
    Button run;
    Globals g;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process_java);

        Bundle data = getIntent().getExtras();
        g = data.getParcelable("Globals");

        output = findViewById(R.id.process_output);

        run = findViewById(R.id.run_btn);
        run.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.run_btn){
            Intent in = new Intent(ProcessJava.this, ProcessExample.class);
            in.putExtra("Globals", g);
            startActivity(in);
        }
    }

    private void process(String s) {
        setTitle(s);
    }
}

