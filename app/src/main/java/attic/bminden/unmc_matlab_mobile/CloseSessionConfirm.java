package attic.bminden.unmc_matlab_mobile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class CloseSessionConfirm extends AppCompatActivity implements View.OnClickListener{

    Button yesBtn, noBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_close_session_confirm);

        yesBtn = findViewById(R.id.yes_btn);
        yesBtn.setOnClickListener(this);
        noBtn = findViewById(R.id.no_btn);
        noBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.yes_btn){
            Intent in = new Intent(CloseSessionConfirm.this, NewSessionConfirm.class);
            startActivity(in);
        }
        if(v.getId() == R.id.no_btn){
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        /** Ignore it **/
    }
}
