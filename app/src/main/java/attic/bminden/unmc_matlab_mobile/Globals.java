package attic.bminden.unmc_matlab_mobile;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;

public class Globals implements Parcelable {
    private int subjectID;
    private int trialNum;
    private String location;

    public Globals(){

    }

    public Globals(int x, int y,String loc){
        subjectID = x;
        trialNum = y;
        location = loc;
    }

    public int getSubjectID() {
        return subjectID;
    }

    public int getTrialNum() {
        return trialNum;
    }

    public String getLocation() { return location; }

    public void setSubjectID(int x){ subjectID = x; }

    public void setTrialNum(int x){ trialNum = x; }

    public void setLocation(String loc){ location = loc;}


    public Globals(Parcel in) {
        subjectID = in.readInt();
        trialNum = in.readInt();
        location = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(subjectID);
        dest.writeInt(trialNum);
        dest.writeString(location);
    }

    public static final Creator<Globals> CREATOR = new Creator<Globals>() {
        @Override
        public Globals createFromParcel(Parcel in) {
            return new Globals(in);
        }

        @Override
        public Globals[] newArray(int size) {
            return new Globals[size];
        }
    };
}
